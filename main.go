package main

import (
	"fmt"
	"os"
	"os/exec"
	"path"
	"strings"
	"text/template"

	"github.com/bh5-go/docopt"
	"github.com/segmentio/go-log"
)

var Version = "0.0.1"

// command line options config
const Usage = `
  Usage:
    go-pages <alias> <real>
    go-pages -h | --help
    go-pages --version
  Options:
    -h, --help       output help information
    -v, --version    output version
`

// template for github pages index.html
const tpl = `
<head>
	<meta http-equiv="refresh" content="0; URL='http://godoc.org/{{.Alias}}'">
	<meta name="go-import" content="{{.Alias}} git http://{{.Real}}.git">
</head>
`

// Stuct that defines paths needed to generate the index.html
type Meta struct {
	Alias string
	Real  string
}

// main program execution
func main() {
	args, err := docopt.Parse(Usage, nil, true, Version, false)
	log.Check(err)
	aPkg := args["<alias>"].(string)
	rPkg := args["<real>"].(string)

	gitCmd("checkout --orphan gh-pages")
	gitCmd("rm -rf .")
	genHTML(Meta{aPkg, rPkg})
	gitCmd("add .")
	gitCmd("commit -amfirst")
	gitCmd("push origin gh-pages")
	gitCmd("checkout master")
}

// Execute git shell commands
func gitCmd(params string) {
	p, _ := exec.LookPath("git")
	cmd := &exec.Cmd{
		Path: p,
		Args: strings.Split("git "+params, " "),
	}

	out, err := cmd.CombinedOutput()
	fmt.Printf("%v", string(out))
	if err != nil {
		panic(err)
	}
}

// Generate index.html for github pages
func genHTML(meta Meta) {
	t := template.New("index.html")
	t, _ = t.Parse(tpl)

	curDir, _ := os.Getwd()

	f, err := os.Create(path.Join(curDir, "index.html"))
	if err != nil {
		panic(err)
	}
	t.Execute(f, meta)
}
